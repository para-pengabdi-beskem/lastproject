from django.apps import AppConfig


class LabProfileConfig(AppConfig):
    name = 'lab_profile'
