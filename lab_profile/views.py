from django.shortcuts import render
from lab_login.models import User
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from lab_login.views import update_profile
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.

response = {}
def index(request):
    html = 'lab_profile/pages/profile.html'
    response['author'] = 'Para Pengabdi Beskem'
    return render(request, html, response)

#========================================================================

def edit_profile(request):
    html = 'lab_profile/edit_profile.html'
    response = get_datas(request)
    return render(request, html, response)

def get_datas(request):
    npm = request.session['kode_identitas']
    user = User.objects.filter(identity_number=npm)
    data = json.loads(serializers.serialize('json', user))
    return data[0]

@csrf_exempt
def update_data(request):
    return HttpResponse(update_profile(request))
