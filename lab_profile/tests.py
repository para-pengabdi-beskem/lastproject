from django.test import TestCase, Client
from django.urls import resolve
from .views import index, edit_profile

# Create your tests here.
class labProfileUnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/lab-profile/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-profile/')
        self.assertEqual(found.func, index)

    #def test_edit_profile_url_is_exist(self):
    #	response = Client().get('/lab-profile/edit_profile/')
    #	self.assertEqual(found.func, edit_profile)