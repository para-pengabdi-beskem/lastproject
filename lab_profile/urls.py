from django.conf.urls import url
from .views import index, edit_profile, update_data

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit_profile/$', edit_profile, name='edit_profile'),
    url(r'^update_data/$', update_data, name='update_data'),
]
