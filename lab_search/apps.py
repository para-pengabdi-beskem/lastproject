from django.apps import AppConfig


class LabSearchConfig(AppConfig):
    name = 'lab_search'
