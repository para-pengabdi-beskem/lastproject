from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class LabSearchTest(TestCase):
    def test_lab_search_url_is_exist(self):
        response = Client().get('/mahasiswa/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/mahasiswa/')
        self.assertEqual(found.func, index)
