$(document).ready(function(){
  var $search = document.getElementsByClassName("form-control")[0];
  $search.setAttribute('id','myInput')
});

function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 100,
        page: 1
    };
}