from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
# Create your views here.
response = {}
def index(request):
    html = 'lab_search/lab_search.html'
    return render(request, html)
