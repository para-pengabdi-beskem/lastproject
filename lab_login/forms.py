from django import forms

# Create your forms here
class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this input',
    }
    
    status_attrs = {
        'type': 'text',
				
        'class': 'status-form-textarea',
        'placeholder':'What is on your mind?...'
    }
    
    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))
