from django.db import models
from django.contrib.postgres.fields import JSONField

# Create your models here
#class Skill(models.Model):
#    SKILL_TYPES = (
#        ('J', 'Java'),
#        ('Py', 'Python'),
#        ('C', 'C'),
#        ('C+', 'C++'),
#        ('P', 'PHP'),
#        ('E', 'English')
#        )
#    SKILL_LEVEL = [
#        ('N', 'Noob'),
#        ('B', 'Beginner'),
#        ('I', 'Intermediate'),
#        ('A', 'Advance'),
#        ('E', 'Expert'),
#        ('L', 'Legend'),
#        ('V', 'Vinny')
#        ]
#    skillType = models.CharField(max_length=20, choices=SKILL_TYPES)
#    skillLevel = models.CharField(max_length=20, choices=SKILL_LEVEL)
    
class User(models.Model):
    identity_number = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200, default="")
    linkedin = models.CharField(max_length=200, default="")
    angkatan = models.CharField(max_length=20)
    picture_link = models.CharField(max_length=200, default="")
#    skills = models.ManyToManyField(Skill)
    skills = models.CharField(max_length=500, default="")
    def __str__(self):  # __unicode__ on Python 2
        return self.name

class Status(models.Model):
    user = models.ForeignKey(User)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

