from django.test import TestCase
from django.test import Client
from django.urls import resolve
from lab_profile.views import update_data
from .views import *
from django.http import HttpRequest

# Create your test here
class labLoginUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/lab-login/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-login/')
        self.assertEqual(found.func, index)

#    def test_update_profile(self):
#    	test = {"name":"John Doe", "email":"john.doe@mail.com", "linkedin":"http://lastprojectppb.herokuapp.com/lab-login/", 
#    			"picture":"https://basecampclimbing.ca/static/basecamp/img/logo.png"}
#    	response = Client().post('update_profile',test)
#
#    	response = Client().get('/update_profile/',test)
#    	html_response = response.content.decode('utf-8')
#
#    	self.assertIn("John Doe", html_response)
#    	self.assertIn("john.doe@mail.com", html_response)
#    	self.assertIn("http://lastprojectppb.herokuapp.com/lab-login/", html_response)
#    	self.assertIn("https://basecampclimbing.ca/static/basecamp/img/logo.png", html_response)

