# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-01-02 13:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_login', '0005_auto_20180101_2028'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='linkedin',
            field=models.CharField(default='', max_length=200),
        ),
    ]
