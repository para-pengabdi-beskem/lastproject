# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-31 03:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_login', '0002_auto_20171229_1716'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='angkatan',
            field=models.CharField(default='kosong', max_length=20),
            preserve_default=False,
        ),
    ]
