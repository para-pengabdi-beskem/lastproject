from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.core import serializers
from .forms import Status_Form
from .models import User, Status
import json

# Create your views here
response = {}
flag = 0
response["author"] = "ParaPengabdiBeskem"
def index(request): 
    if 'user_login' in request.session:
        global flag
        if(flag == 1):
            flag = 0
        return HttpResponseRedirect(reverse('lab-login:dashboard'))
    else:
        html = 'lab_login/login.html'
        return render(request, html, response)
    
def dashboard(request): 
    if 'user_login' in request.session:
        response['is_login'] = True

        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        
        response['user'], is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username, angkatan="20"+kode_identitas[0:2])
        my_Status= Status.objects.filter(user_id=response['user'].id)
        response['all_status'] = Status.objects.filter(user_id=response['user'].id)
        response['Status']= response['all_status'][::-1]

        if len(my_Status)== 0:
            response['last_status']='-'
        else:
            response['last_Status']=response['Status'][0]

        response['status_count'] = response['all_status'].count()
        response['status_form'] = Status_Form

        set_data_for_session(response,request)
        html = 'lab_login/dashboard.html'
        return render(request, html, response)
    
    response['is_login'] = False
    html = 'lab_login/login.html'
    return render(request, html, response)

def set_data_for_session(res, request):
    global flag
    if(flag == 0):
        #=========[FOR PROFILE]=========#
        request.session['nama'] = 'kosong'
        request.session['keahlian'] = 'kosong'
        request.session['email'] = 'kosong'
        request.session['linkedin'] = 'kosong'
        request.session['gambar'] = "https://d30y9cdsu7xlg0.cloudfront.net/png/363633-200.png"
        #===============================#
        flag = 1

    response['nama'] = request.session['nama']
    response['keahlian'] = request.session['keahlian']
    response['email'] = request.session['email']
    response['linkedin'] = request.session['linkedin']
    response['gambar'] = request.session['gambar']

    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def post_status(request): 
    form = Status_Form(request.POST or None)
    if 'user_login' in request.session and request.method == 'POST' and form.is_valid():
        response['is_login'] = True
        
        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        status_message = request.POST['status']
        response['user'], is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username, angkatan="20"+kode_identitas[0:2])
        new_status = Status.objects.create(description=status_message, user=response['user'])
        return HttpResponseRedirect(reverse('lab-login:dashboard'))
        
    response['is_login'] = False
    html = 'lab_login/login.html'
    return render(request, html, response)

def get_user_list(request):
    if request.method == 'GET':
        user_list = User.objects.all()
        data = serializers.serialize('json', user_list)
        return HttpResponse(data)

def update_profile(request):
    if request.method == 'POST':
        npm = request.session["kode_identitas"]
        user = User.objects.get(identity_number = npm)
        data = json.loads(list(request.POST.keys())[list(request.POST.values()).index('')])

        user.name = data['name']
        user.email = data['email']
        user.linkedin = data['linkedin']
        user.picture_link = data['picture']
        user.skills = data['skills']

        request.session['nama'] = user.name
        request.session['email'] = user.email
        request.session['linkedin'] = user.linkedin
        request.session['gambar'] = user.picture_link
        request.session['keahlian'] = user.skills

        print(data)
        print(data['skills'])
        
        user.save()         # ERROR
        user_list = User.objects.all()
        a = serializers.serialize('json', user_list)
        print(a)

    return JsonResponse({"success":"true"})
