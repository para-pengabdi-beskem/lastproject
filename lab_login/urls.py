from django.conf.urls import url
from .views import *
from .custom_auth import auth_login, auth_logout

# Create your urls here
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^post_status', post_status, name='post-status'),
    url(r'^get-user-list/$', get_user_list, name='get-user-list'),
    url(r'^update_profile/$', update_profile, name='update_profile'),
    
]
