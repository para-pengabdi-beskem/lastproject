"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
from django.contrib import admin
import lab_search.urls as lab_search
import lab_profile.urls as lab_profile
import lab_login.urls as lab_login
import lab_history.urls as lab_history

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^mahasiswa/', include(lab_search, namespace='lab-search')),
    url(r'^lab-profile/', include(lab_profile, namespace='lab-profile')),
    url(r'^lab-login/', include(lab_login, namespace='lab-login')),
	url(r'^lab-history/', include(lab_login, namespace='lab-history')),
    url(r'^$', RedirectView.as_view(url = 'lab-login/', permanent=True), name='index'),
]
