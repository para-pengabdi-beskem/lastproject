from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from lab_login.views import set_data_for_session
from lab_profile.views import get_datas

response = {}

# Create your views here.
def index(request):
    if 'user_login' in request.session:
        response['login'] = True
        set_data_for_session(response, request)
        response["mahasiswa"] = get_datas(request)		
        return render(request, 'history.html', response)
    else:
        response['login'] = False
        return render(request, 'login.html', response)

