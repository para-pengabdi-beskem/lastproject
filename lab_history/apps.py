from django.apps import AppConfig


class LabHistoryConfig(AppConfig):
    name = 'lab_history'
